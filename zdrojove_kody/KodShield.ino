#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>

// Inicializace I2C LCD displeje
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Definice pinů
#define LED1_PIN 11            // LED1 pro indikaci světla
#define LED2_PIN 12            // LED2 pro indikaci světla
#define LED_STRIP_PIN 10       // Pin pro RGB LED pásek
#define NUM_LEDS 8             // Počet LED na pásku
#define PHOTO_PIN A0           // Pin pro fotorezistor
#define TEMP_SENSOR_PIN 7      // Pin pro teplotní senzor (DS18B20)

// Inicializace RGB LED pásku
Adafruit_NeoPixel strip(NUM_LEDS, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

// Inicializace teplotního senzoru
OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensors(&oneWire);

// Proměnné
int photoValue = 0;         // Hodnota z fotorezistoru
float temperatureC = 0.0;   // Hodnota teploty

void setup() {
  // Inicializace LCD displeje
  lcd.init();
  lcd.backlight();
  
  // Inicializace pinů pro LEDky
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);
  
  // Inicializace LED pásku
  strip.begin();
  strip.show();  // Zajistí, že všechny LEDky jsou zhasnuté
  
  // Inicializace teplotního senzoru
  sensors.begin();
}

void readSensors() {
  // Čtení hodnoty z fotorezistoru (úroveň světla)
  photoValue = analogRead(PHOTO_PIN);

  // Požadavek na čtení teploty z DS18B20 senzoru
  sensors.requestTemperatures();
  temperatureC = sensors.getTempCByIndex(0);
}

void displayValues() {
  // Vymazání displeje a zobrazení hodnot světla a teploty
  lcd.clear();
  
  // Zobrazení hodnoty světla (0-1023)
  lcd.setCursor(0, 0);
  lcd.print("Svetlo: ");
  lcd.print(photoValue);
  
  // Zobrazení hodnoty teploty ve stupních Celsia
  lcd.setCursor(0, 1);
  lcd.print("Teplota: ");
  lcd.print(temperatureC);
  lcd.print(" C");
}

void controlLEDs() {
  // Ovládání LEDek na základě hodnoty z fotorezistoru:
  // Pokud je hodnota světla nízká, zapne se LED1 a LED2 zhasne (tmavší prostředí)
  // Pokud je hodnota světla vysoká, zapne se LED2 a LED1 zhasne (světlejší prostředí)
  
  if (photoValue < 512) {  // Nízká úroveň světla
    digitalWrite(LED1_PIN, HIGH);  // Zapne LED1
    digitalWrite(LED2_PIN, LOW);   // Zhasne LED2
  } else {  // Vysoká úroveň světla
    digitalWrite(LED1_PIN, LOW);   // Zhasne LED1
    digitalWrite(LED2_PIN, HIGH);  // Zapne LED2
  }
}

void controlRGBStrip() {
  // Změna barvy RGB pásku na základě teploty:
  // - Chladno (modrá) pokud je teplota pod 20°C
  // - Teplo (zelená) pokud je teplota mezi 20°C a 30°C
  // - Horko (červená) pokud je teplota nad 30°C
  
  uint32_t color;
  
  if (temperatureC < 20.0) {
    color = strip.Color(0, 0, 255);  // Modrá pro chladné prostředí
  } else if (temperatureC >= 20.0 && temperatureC <= 30.0) {
    color = strip.Color(0, 255, 0);  // Zelená pro teplé prostředí
  } else {
    color = strip.Color(255, 0, 0);  // Červená pro horké prostředí
  }

  // Nastavení barvy všech LEDek na pásku
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, color);
  }
  
  // Aktualizace pásku s novou barvou
  strip.show();
}

void loop() {
  // Čtení dat ze senzorů
  readSensors();
  
  // Zobrazení hodnot na LCD displeji
  displayValues();
  
  // Ovládání LEDek na základě úrovně světla
  controlLEDs();
  
  // Ovládání RGB pásku na základě teploty
  controlRGBStrip();
  
  // Krátké zpoždění, aby se předešlo blikání
  delay(500);
}
