# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 20h |
| jak se mi to podařilo rozplánovat | Dlouho jsem čekal na součástky a většinu věcí jsem dělal na poslední chvíli |
| design zapojení | <a href="https://gitlab.spseplzen.cz/moravecjan/4r-vlastni-shield/-/blob/main/dokumentace/design/Design.png" target="_blank">Design</a>|
| proč jsem zvolil tento design | Přijde mi lepší pokud mohu se součástkami více manipulovat |
| z jakých součástí se zapojení skládá | 2x led, 1x RGB pásek, 1x Display, 4x rezistor, Vodiče, 1x i2C převodník pro LCD displej, 1x fotorezistor, DS18B20 čidlo kabel |
| realizace | <a href="https://gitlab.spseplzen.cz/moravecjan/4r-vlastni-shield/-/blob/main/dokumentace/fotky/44AD8D8B-2199-4ECE-80F5-2BD6B076CD69.jpeg" target="_blank">Realizace</a> |
| nápad, v jakém produktu vše propojit dohromady|  |
| co se mi povedlo | Připojovací součástky |
| co se mi nepovedlo/příště bych udělal/a jinak | Objednal bych součástky dříve, abych měl rezervu i kdyby se opozdily |
| zhodnocení celé tvorby | Začátek dobrý, ale poté se mi to bohužel nepovedlo plně zprovoznit |
